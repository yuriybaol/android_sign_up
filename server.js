﻿var express     = require('express');
var app         = express();
var fs = require('fs');
var https = require('https'); //
var bodyParser  = require('body-parser');
var morgan      = require('morgan');
var mongoose    = require('mongoose');
var passport	  = require('passport');
var config      = require('./config/database'); // get db config file
var User        = require('./app/models/user'); // get the mongoose model
var port 	      = 433;
var jwt 			  = require('jwt-simple');
var firebase = require('firebase');
var app_firebase = firebase.initializeApp({
  serviceAccount: "login2-a0d00e55215c.json",
  
});

//var GOOGLE_CLIENT_ID = '690067802690-fql1852dh3jnkgfet0a111fca5sh9qrp.apps.googleusercontent.com';
var GOOGLE_CLIENT_ID = '690067802690-g3mcm2khls0ec0solqmkoev42hc4ob8c.apps.googleusercontent.com'
var GoogleTokenStrategy = require('passport-google-id-token');
var verifier = require('google-id-token-verifier');

// get our request parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// log to console
app.use(morgan('dev'));

// Use the passport package in our application
app.use(passport.initialize());


// demo Route (GET http://localhost:8080)
app.get('/', function(req, res) {
	res.json({success: true, msg: 'con ok'});
  //res.send('Hello! The API is at http://localhost:' + port + '/api');
});

https.createServer({
    key  : fs.readFileSync('server.key'),
	cert : fs.readFileSync('server.crt')
    }, app).listen(443);
	
mongoose.connect(config.database);

require('./config/passport')(passport);

var apiRoutes = express.Router();

apiRoutes.post('/signup', function(req, res) {
  if (!req.body.name || !req.body.password) {
    res.json({succes: false, msg: 'Please pass name and password.'});
  } else {
    var newUser = new User({
      name: req.body.name,
      password: req.body.password
    });
    newUser.save(function(err) {
      if (err) {
        res.json({success: false, msg: 'Username already exists.'});
      } else {
        res.json({success: true, msg: 'Successful created user!'});
      }
    });
  }
});

apiRoutes.post('/authenticate', function(req, res) {
	
  User.findOne({
    name: req.body.name
  }, function(err, user) {
    if (err) throw err;

    if (!user) {
      res.send({success: false, msg: 'Authentication failed. User not found.'});
    } else {
      user.comparePassword(req.body.password, function(err, isMatch) {
        if (isMatch && !err) {
          var token = jwt.encode(user, config.secret);
          res.json({success: true, msg: 'Authentication correct!',  token: 'JWT ' + token});
        } else {
          res.send({success: false, msg: 'Authentication failed.'});
        }
      });
    }
  });
});


apiRoutes.post('/memberinfo', passport.authenticate(['google-id-token', 'jwt', 'facebook-token', 'twitter-token'], {session: false}),
  function (req, res) {

	var IdToken = req.body.id_token;
	if(IdToken){

	verifier.verify(IdToken, GOOGLE_CLIENT_ID, function (err, tokenInfo) {
  if (!err) {
    // use tokenInfo in here. 
     res.json({success: true, msg: 'Welcome in the member area ' + tokenInfo.given_name + '!'});
	 return;
  }
  else{
		 res.json({success: false, msg: 'Unauthorized!'});
		 return;
  }
});
return;
}

var facebookIdToken = req.body.access_token;
if(facebookIdToken){
	
	  if (req.user){
		 
                //you're authenticated! return sensitive secret information here.
                res.status(200).send({'success':'true','msg':'Welcome in the area' + req.user.name});
            } else {
                // not authenticated. go away.
                res.send(401)
            }
	
			 //res.json({success: true, msg: 'Esto no tira ni de broma hulio!'});

}
var twitterOauthToken = req.body.oauth_token;
if(twitterOauthToken){
		  if (req.user){
		 
                //you're authenticated! return sensitive secret information here.
                res.status(200).send({'success':'true','msg':'Welcome in the area ' + req.user.name});
            } else {
                // not authenticated. go away.
                res.send(401);
			}
            
	
}

else{

var token = getToken(req.headers);
  if (token) {
    var decoded = jwt.decode(token, config.secret);
    User.findOne({
      name: decoded.name
    }, function(err, user) {
      if (err) throw err;

      if (!user) {
        return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'});
      } else {
        return res.json({success: true, msg: 'Welcome in the member area ' + user.name + '!'});
      }
    });
  } else {
    return res.status(403).send({success: false, msg: 'No token provided.'});
  }


}

});


getToken = function(headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

app.use('/api', apiRoutes);

// Start the server
app.listen(port);
console.log('There will be dragons: https://localhost:' + port);
