var JwtStrategy = require('passport-jwt').Strategy;
var GoogleTokenStrategy = require('passport-google-id-token');
var FacebookTokenStrategy = require('passport-facebook-token');
var TwitterTokenStrategy = require('passport-twitter-token');

// load up the user model
var User = require('../app/models/user');
var config = require('../config/database'); // get db config file

var bodyParser  = require('body-parser');
var math = require('mathjs');

//var GOOGLE_CLIENT_ID = '690067802690-fql1852dh3jnkgfet0a111fca5sh9qrp.apps.googleusercontent.com

var GOOGLE_CLIENT_ID = '690067802690-g3mcm2khls0ec0solqmkoev42hc4ob8c.apps.googleusercontent.com';
var FACEBOOK_APP_ID = '1353765721331018';
var FACEBOOK_APP_SECRET = 'fd3c7475bc0b80b7e1d1e5cc2663279d';
var TWITTER_CONSUMER_KEY = 'bSw6Zyh3QNbLo3rdK4S4GfloF';
var TWITTER_CONSUMER_SECRET = 'UvxcedgyglTKPZgXcCt1mzIjjkCM3Fy8UPBsXe6SQVBWozPlTv';

module.exports = function(passport) {
  var opts = {};
  opts.secretOrKey = config.secret;
  passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
    User.findOne({id: jwt_payload.id}, function(err, user) {
          if (err) {
              return done(err, false);
          }
          if (user) {
              done(null, user);
          } else {
              done(null, false);
          }
      });
  }));

passport.use(new GoogleTokenStrategy({
    clientID: GOOGLE_CLIENT_ID,
    
    //getGoogleCerts: optionalCustomGetGoogleCerts
  },
 
  function(parsedToken, googleId, done) {
    User.findOne({ name:  googleId}, function (err, user) 
	{

      if(!err)
	  {
            
		if(!user){
		var newUser = new User({
		name: googleId,
		password: math.random().toString(36).slice(-8)
		});
		newUser.save(function(err) {
		if (err)
			{
       // res.json({success: false, msg: 'Username already exists.'});
			} 
		else
			{
        //res.json({success: true, msg: 'Successful created user!'});
			}
			});
    
		return done(null,newUser);

		}
		return done(err, user);
    }
    
	//return done(null, user);

    });
	
  }));
  
  passport.use(new FacebookTokenStrategy({
    clientID: FACEBOOK_APP_ID,
    clientSecret: FACEBOOK_APP_SECRET
  }, 
   
  function(accessToken, refreshToken, profile, done) {	 
	     var user1 = {
        'email': profile.emails[0].value,
        'name' : profile.name.givenName + profile.name.familyName,
        'id'   : profile.id,
        'token': accessToken
    }

    User.findOne({ name:  profile.id}, function (err, user) 
	{
      if(!err)
	  {
            
		if(!user){
		var newUser = new User({
		name: profile.id,
		password: math.random().toString(36).slice(-8)
		});
		newUser.save(function(err) {
		if (err)
			{
       // res.json({success: false, msg: 'Username already exists.'});
			} 
		else
			{
        //res.json({success: true, msg: 'Successful created user!'});
			}
			});
    
		return done(null,newUser);

		}
		return done(err, user1);
    }
    return done(error, user);
	//return done(null, user);

    });
  }
  
));

passport.use(new TwitterTokenStrategy({
    consumerKey: TWITTER_CONSUMER_KEY,
    consumerSecret: TWITTER_CONSUMER_SECRET
  }, function(token, tokenSecret, profile, done) {
	    var user2 = {
        'name' : profile.username,
        'id'   : profile.id,
    }

    User.findOne({ name:  profile.id}, function (err, user) 
	{
      if(!err){   
		if(!user){
		var newUser = new User({
		name: profile.id,
		password: math.random().toString(36).slice(-8)
		});
		newUser.save(function(err) {
		if (err){//res.json({success: false, msg: 'Username already exists.'});
			}else{//res.json({success: true, msg: 'Successful created user!'});
			}
			});
		return done(null,newUser);
		}
		return done(err, user2);
    }
    return done(error, user);
	//return done(null, user);
    });
  }
));

};
